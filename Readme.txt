AutocompleteTreeComboBox:
    A combobox that shows a tree when you open it. When you type in the
    text field, it will filter all the items in the tree on the fly, and only
    show the matches and their parent nodes.
    As soon as only one match is left for what you have typed, this one item
    will be selected.
    The implementation inherits from ComboCtrl for its layout and basic
    functionality. The drop down frame showing the tree is custom.

Python ver 2.7

Prerequisites: Make sure you have wxPython installed
                   (http://www.wxpython.org/)
