"""This module contains an autocomplete tree comboBox and private supporting
classes.
"""

import wx.combo
from wx.lib.newevent import NewEvent

# Bind to this event to know when the selection changes:
COMBO_SELECTION_CHANGED_EVENT, EVT_SELECTION_CHANGED = NewEvent()

class AutocompleteTreeComboBox(wx.combo.ComboCtrl):
    """A combobox that shows a tree when you open it. When you type in the
    text field, it will filter all the items in the tree on the fly, and only
    show the matches and their parent nodes.
    As soon as only one match is left for what you have typed, this one item
    will be selected.
    The implementation inherits from ComboCtrl for its layout and basic
    functionality, but the entire drop down frame showing the tree is custom.

    """
    def __init__(self, parent):
        wx.combo.ComboCtrl.__init__(self, parent)
        self.SetButtonPosition(side=wx.LEFT)
        self.SetPopupControl(_DummyComboPopup())

        # Elements:
        self.tree_combo_tree_popup = _TreeComboBoxTreePopup(self)

        # Variables:
        self.selected_item = None
        # The data to select from, this should never be edited:
        self.tree_data = None
        # The data visible in the tree, initially all data
        self.tree_data_filtered = None
        # Letters to neglect when typing, after unique selection is made
        self.keys_to_neglect = ""

        # We need a reference to the root frame to detect movement
        frame = self.Parent
        while not isinstance(frame, wx.Frame):
            frame = frame.Parent

        # Events on the frame:
        frame.Bind(wx.EVT_MOVE, self._on_move)
        # Events on this control:
        self.Bind(wx.EVT_SIZE, self._on_size_change)
        self.Bind(wx.EVT_KILL_FOCUS, self._on_kill_focus)
        self.Bind(wx.EVT_TEXT, self._on_text_changed)
        # Events on the text control:
        self.GetTextCtrl().Bind(wx.EVT_KEY_DOWN, self._on_text_ctrl_key_down)
        # Events on the popup:
        self.tree_combo_tree_popup.Bind(EVT_TREE_POPUP_SEL_MADE,
                                        self._on_selection_made)
        self.tree_combo_tree_popup.Bind(EVT_TREE_POPUP_MOVE_OUTS,
                                        self._on_move_outside_of_tree)

    # The only real public method:
    def set_items(self, data):
        """Set the data for the tree in the combo box.

        Arguments:
        data -- the data to be represented by the tree
        The data can be an object of any type, the only requirement is
          - a property "children" containing a list of the child nodes
          - an implementation of the __str__ method, used for visualization
        Also note that the root node will not be displayed

        """
        self.tree_data = data
        self.tree_data_filtered = self._filter(data, "")[1]
        self._update_data_visible_in_tree(self.tree_data_filtered)

    def OnButtonClick(self, *args, **kwargs):
        """Override of the click on the drop down button on the combo control
        This makes certain that we don't use the popup control from the combo
        control, but instead the custom tree popup.

        """
        self._toggle_tree_popup()

    def _on_text_changed(self, evt):
        """Filter the tree depending on the content of the text control.
        If only one choice is left, select it.

        """
        filter_txt = evt.EventObject.GetTextCtrl().GetValue()
        count, self.tree_data_filtered = self._filter(self.tree_data,
                                                      filter_txt)
        self._update_data_visible_in_tree(self.tree_data_filtered)

        if self.tree_data_filtered and not filter_txt == "" and count > 1:
            self._show_tree_popup_if_hidden()
            self.tree_combo_tree_popup.expand_all()
        else:
            self._hide_tree_popup()

        if count == 0: # No results
            self._turn_red()
        elif count == 1: # Exactly one result
            selected_item = self._get_first_leaf(self.tree_data_filtered)
            selected_string = str(selected_item).upper()
            end = selected_string.find(filter_txt.upper()) + len(filter_txt)
            self.keys_to_neglect = selected_string[end:].upper()
            self._set_selection(selected_item)
        else:
            self._turn_grey()

    def _get_first_leaf(self, node):
        """Recursively traverses the node and returns the first leaf that is
        encountered.
        The returned leaf is the original node.

        """
        if not node.children:
            return node.original_node
        else:
            return self._get_first_leaf(node.children[0])

    def _filter(self, original_node, filter_txt):
        """Recursively filter the children of a node and return a filterednode
        containing a list of its children after applying the filter and a
        reference to the original non-filtered node.

        We return something if one of the child nodes contains the filter.
        (This method will "neglect" the root node.)

        Returns a tuple with the amount of actual matches and the filtered
        node.

        """
        filter_txt = filter_txt.upper() # We compare upper to upper.
        filtered_children = []
        amount_of_matches = 0

        for child_original_node in original_node.children:
            count, child_filtered_node = self._node_filter(child_original_node,
                                                           filter_txt)
            if child_filtered_node:
                filtered_children.append(child_filtered_node)
                amount_of_matches += count

        if filtered_children:
            return amount_of_matches, _FilteredTreeNode(original_node,
                                                        filtered_children,
                                                        False)
        else:
            return 0, None

    def _node_filter(self, original_node, filter_txt):
        """Recursively filter the children of a node and return a filterednode
        containing a list of its children after applying the filter and a
        reference to the original non-filtered node.

        We return something if
          - either the node contains the filter
          - or one of the child nodes contains the filter

        """
        filtered_children = []
        amount_of_matches = 0

        for child_original_node in original_node.children:
            count, child_filtered_node = self._node_filter(child_original_node,
                                                           filter_txt)
            if child_filtered_node:
                filtered_children.append(child_filtered_node)
                amount_of_matches += count

        contains_filter = filter_txt in str(original_node).upper()

        if contains_filter:
            amount_of_matches += 1
        # "Select" means that in the tree the node has to be "highlighted"
        # We don't select when no filter was applied:
        select = contains_filter and not filter_txt == ""

        if contains_filter or filtered_children:
            return amount_of_matches, _FilteredTreeNode(original_node,
                                                        filtered_children,
                                                        select)
        else:
            return 0, None

    def _update_data_visible_in_tree(self, data):
        """Updates the visible data in the tree, but keeps the focus on the
        text box.

        """
        self.tree_combo_tree_popup.set_items(data)
        self._restore_focus_to_text_ctrl()

    def _on_selection_made(self, evt):
        """Hides the tree popup and selects the text in the text box."""
        self._hide_tree_popup()
        self._set_selection(evt.selected_item)

    def _set_selection(self, selected_item):
        """Updates the selected_item, displays it and fires the event.
        This method should always be used to update self.selected_item

        """
        self.selected_item = selected_item
        # Here we do something special:
        # We want to change the text, but we don't want this to trigger any
        # other event.
        # So we temporarily unbind the EVT_TEXT event:
        self.Unbind(wx.EVT_TEXT)
        # Then we change the text:
        self.GetTextCtrl().SetValue(str(selected_item))
        # We select the entire text:
        string_lenght = len(str(selected_item))
        self.GetTextCtrl().SetInsertionPoint(string_lenght)
        self.GetTextCtrl().SetSelection(0, string_lenght)
        # And then we bind the EVT_TEXT event again:
        self.Bind(wx.EVT_TEXT, self._on_text_changed)
        self._turn_white()
        # We also remove the filter, there's no point in keeping it
        self.set_items(self.tree_data)
        # The selection has changed! The entire world has to know!
        evt = COMBO_SELECTION_CHANGED_EVENT()
        evt.selected_item = selected_item
        wx.PostEvent(self, evt)

    def _on_text_ctrl_key_down(self, evt):
        """Act on any keyboard input in the text control.
        When a new item is selected because a piece of text was entered that
        uniquely identifies the item, the complete string is selected in the
        text control. To prevent that, while continuing to type in the
        selected string, the entire string would be deleted (because it got
        selected), we here neglect the rest of the characters that would be
        typed, as long as they correspond to what was left to type of the
        selected string.

        """
        if (self.keys_to_neglect and evt.KeyCode < 256 and
              chr(evt.KeyCode) == self.keys_to_neglect[0]):
            self.keys_to_neglect = self.keys_to_neglect[1:]
        else:
            self.keys_to_neglect = ""
            if evt.KeyCode in [wx.WXK_DOWN, wx.WXK_NUMPAD_DOWN]:
                if self.tree_data_filtered:
                    self.tree_combo_tree_popup.select_first()
                    self._show_tree_popup_if_hidden()
                    self.tree_combo_tree_popup.SetFocus()
            elif evt.KeyCode in [wx.WXK_UP, wx.WXK_NUMPAD_UP]:
                pass
            elif evt.KeyCode == wx.WXK_ESCAPE:
                self.GetTextCtrl().Clear()
            elif (evt.KeyCode == wx.WXK_RETURN or
                  evt.KeyCode == wx.WXK_NUMPAD_ENTER):
                self._hide_tree_popup()
            else:
                evt.Skip()

    def _on_move_outside_of_tree(self, evt):
        """Restores the focus to the text box, e.g. when moving the selection
        outside of the tree with the arrows.

        """
        self._restore_focus_to_text_ctrl()
        evt.Skip()

    def _on_move(self, evt):
        """Move the tree popup along with the main window when it moves."""
        self._set_tree_popup_dimensions()
        evt.Skip()

    def _on_size_change(self, evt):
        """Resize the tree popup along with the combobox when its size
        changes.

        """
        self._set_tree_popup_dimensions()
        evt.Skip()

    def _on_kill_focus(self, evt):
        """Hide the tree popup window when the user clicks somewhere else.
        We don't hide the popup when
        - We move the focus to the popup itself
        - We move the focus back from the popup to the combobox

        """
        if not (self.tree_combo_tree_popup.IsActive() or
                self.FindFocus() is self):
            self._hide_tree_popup()
        evt.Skip()

    def _toggle_tree_popup(self):
        """Shows or hides the tree popup."""
        if self.tree_combo_tree_popup.IsShown():
            self._hide_tree_popup()
        else:
            self._show_tree_popup()

    def _show_tree_popup_if_hidden(self):
        """Shows the tree popup only if it's currently not visible."""
        if not self.tree_combo_tree_popup.IsShown():
            self._show_tree_popup()

    def _show_tree_popup(self):
        """Opens the tree popup, but keeps the focus on the combobox text
        control.

        """
        self._set_tree_popup_dimensions()
        # temporarily unbind the EVT_KILL_FOCUS event:
        self.Unbind(wx.EVT_KILL_FOCUS)
        self.tree_combo_tree_popup.Show()
        self._restore_focus_to_text_ctrl()
        # bind it again
        self.Bind(wx.EVT_KILL_FOCUS, self._on_kill_focus)

    def _restore_focus_to_text_ctrl(self):
        """Puts the focus on the text box, but keeps the current selection."""
        # Remember cursor and selection state
        cursor = self.GetTextCtrl().GetInsertionPoint()
        sel_from, sel_to = self.GetTextCtrl().GetSelection()
        # Set focus back
        self.GetTextCtrl().SetFocus()
        # Put cursor and selection state back
        self.GetTextCtrl().SetInsertionPoint(cursor)
        self.GetTextCtrl().SetSelection(sel_from, sel_to)

    def _hide_tree_popup(self):
        """Hides the tree popup."""
        self.tree_combo_tree_popup.Hide()

    def _set_tree_popup_dimensions(self):
        """Automatically sets the location and dimensions of the tree popup
        window, depending on the size and location of the combo text box.

        """
        pos_x = self.GetScreenPosition().x
        pos_y = self.GetScreenPosition().y + self.GetSize().height
        width = self.GetSize().width
        height = 300
        self.tree_combo_tree_popup.SetDimensions(pos_x, pos_y, width, height)

    def _turn_white(self):
        """Colors the background of the combo text box white."""
        self.GetTextCtrl().SetBackgroundColour(wx.WHITE)

    def _turn_grey(self):
        """Colors the background of the combo text box grey."""
        self.GetTextCtrl().SetBackgroundColour(wx.Colour(200, 200, 200))

    def _turn_red(self):
        """Colors the background of the combo text box red."""
        self.GetTextCtrl().SetBackgroundColour(wx.Colour(255, 200, 200))


class _FilteredTreeNode():
    """A wrapper class for the filtered tree that we show in the tree control

    The tree combo box will keep track of the "full" tree, but it will only
    hand over a filtered version of the tree to the tree control.
    The filtered version will contain a list of all child nodes that remain
    after applying the filter, but it will also keep a reference to the
    original node, which contains a list of all the children before the filter
    was applied.
    In the filtered tree it's also tracked if a node is selected, i.e. it
    matches to the filter. Remember: a node can be part of a tree after
    applying a filter, because either it's a direct match or because any of
    the children match.

    """
    def __init__(self, original_node, children, select):
        self.children = children
        self.original_node = original_node
        self.select = select

    def __str__(self):
        return str(self.original_node)


TREE_POPUP_SEL_MADE_EVT, EVT_TREE_POPUP_SEL_MADE = wx.lib.newevent.NewEvent()
TREE_POPUP_MOVE_OUTS_EVT, EVT_TREE_POPUP_MOVE_OUTS = wx.lib.newevent.NewEvent()


class _TreeComboBoxTreePopup(wx.Frame):
    """Popup window for the combobox showing the tree.

    This is implemented as a separate frame, floating on top of the parent
    window.
    The combo box that pops this open must make certain that this frame is
    moved or resized when the main window is moved or resized.

    """
    def __init__(self, parent):
        wx.Frame.__init__(self, parent,
                          style=wx.FRAME_FLOAT_ON_PARENT|wx.FRAME_NO_TASKBAR)

        # Elements:
        self.tree = wx.TreeCtrl(self, style=wx.TR_HIDE_ROOT # no root node
                                |wx.TR_HAS_BUTTONS # + and - buttons
                                |wx.TR_MULTIPLE # multiselect
                                |wx.TR_LINES_AT_ROOT # lines between root
                                    # nodes, need this for the buttons anyway
                                |wx.TR_TWIST_BUTTONS) # for windows style

        # Events:
        self.tree.Bind(wx.EVT_TREE_KEY_DOWN, self._on_key_down)
        self.tree.Bind(wx.EVT_LEFT_DOWN, self._on_left_down)

    def set_items(self, data):
        """Set the (filtered) tree nodes to be displayed by the tree and
        select them if necessary.

        data -- a tree of objects of type _FilteredTreeNode

        """
        self.tree.DeleteAllItems()
        if data:
            parent_tree_node = self.tree.AddRoot(str(data))
            self._add_child_items(parent_tree_node, data)
            self.tree.Refresh()

    def _add_child_items(self, parent_tree_node, parent_data_node):
        """Recursive implementation used by set_items"""
        for child_data_node in parent_data_node.children:
            tree_node_data = wx.TreeItemData(child_data_node)
            child_tree_node = self.tree.AppendItem(parent_tree_node,
                                                   str(child_data_node),
                                                   data = tree_node_data)
            if child_data_node.select:
                self.tree.SelectItem(child_tree_node, True)
            self._add_child_items(child_tree_node, child_data_node)

    def expand_all(self):
        """Expand all the nodes visible in the tree.
        Typically used to make certain that all the results of a search are
        visible.

        """
        self.tree.ExpandAll()

    def select_first(self):
        """Select the first item of the tree.
        Intended to be used if you enter the tree by pressing the down arrow
        from the text control.

        """
        self.tree.UnselectAll()
        self.tree.ClearFocusedItem()
        self.tree.SelectItem(self.tree.GetFirstVisibleItem())

    def _on_key_down(self, evt):
        """Act on keyboard input on the tree."""
        if evt.KeyCode in [wx.WXK_RETURN, wx.WXK_NUMPAD_ENTER]:
            tree_item_id = self.tree.GetSelections()[0]
            item = self.tree.GetItemData(tree_item_id).GetData()
            self._on_item_selected(item)
        elif evt.KeyCode == wx.WXK_ESCAPE:
            self.Hide()
        elif evt.KeyCode in [wx.WXK_UP, wx.WXK_NUMPAD_UP]:
            if self.tree.GetFocusedItem() == self.tree.GetFirstVisibleItem():
                evt = TREE_POPUP_MOVE_OUTS_EVT()
                wx.PostEvent(self, evt)
        evt.Skip()

    def _on_left_down(self, evt):
        """Act on mouse input on the tree."""
        evt.Skip()
        tree_item_id, where = self.tree.HitTest(evt.GetPosition())
        if where == wx.TREE_HITTEST_ONITEMLABEL:
            # only when we really clicked on the label (not on expand/collapse)
            item = self.tree.GetItemData(tree_item_id).GetData()
            self._on_item_selected(item)

    def _on_item_selected(self, item):
        """Act on the selection of an item in the tree."""
        evt = TREE_POPUP_SEL_MADE_EVT()
        evt.selected_item = item
        wx.PostEvent(self, evt)



class _DummyComboPopup(wx.combo.ComboPopup):
    """Dummy ComboPopup, implemented because the ComboCtrl is still doing some
    assert methods from time to time, even though the popup control is never
    explicitely used and replaced by a custom implementation.
    This dummy popup control is still needed for the asserts to pass.
    It is not meant to do anything.

    """
    def __init__(self):
        wx.combo.ComboPopup.__init__(self)
        self.tree = None

    # Mandatory method implementation for dummy ComboPopup
    def Create(self, parent, *args, **kwargs):
        self.tree = wx.TreeCtrl(parent)

    # Mandatory method implementation for dummy ComboPopup
    def GetControl(self, *args, **kwargs):
        return self.tree
