"""Demo application for auto complete tree combobox."""

import wx

from autocompletetreecombobox import AutocompleteTreeComboBox
from autocompletetreecombobox import EVT_SELECTION_CHANGED

class SomeCustomTreeNode():
    """Just some custom tree node that can be used in the tree combo box
    The properties some_id and some_label are of no importance.
    The only thing we need is a list of children and an implementation of the
    __str__ method.
    (See the set_items method of AutocompleteTreeComboBox)

    """
    def __init__(self, some_id, some_label):
        self.children = []
        self.some_id = some_id
        self.some_label = some_label

    def add_child(self, child_node):
        """Adds child node to the node."""
        self.children.append(child_node)
        return child_node

    def remove_child(self, child_node):
        """Removes child node from the node."""
        self.children.remove(child_node)
        return child_node

    def __str__(self):
        return self.some_label


class MainWindow(wx.Frame):
    """Main window to demonstrate the autocomplete tree combo box.

    It displays an autocomplete tree combo box and loads some data.
    Each time an object is selected in the tree combo box, it is displayed in
    the text control underneath.

    """
    def __init__(self):
        title = "A simple demo for a wxPython autocomplete tree combo box"
        wx.Frame.__init__(self, None, title=title, size=(640, 320))

        # Elements:
        label_intro = ("An autocomplete tree combo box:\nFind and select any "
                       "item from a tree, without even having to expand it, "
                       "just by typing any part of it.")
        static_text_intro = wx.StaticText(self, label=label_intro)
        label_prereq = "Prerequisites: Python 2.x, wxPython"
        static_text_prereq = wx.StaticText(self, label=label_prereq)
        label_for_combo = ("Try typing some strings like \"red\", \"green\", "
                           "\"greenish\", \"bl\", \"bla\", \"black\",... and"
                           " see what happens.\nAlso try using the arrow keys"
                           " to go play in the tree.\nAnd see what happens if"
                           " you resize or move the screen with the combo"
                           " open.")
        static_text_for_combo = wx.StaticText(self, label=label_for_combo)
        auto_complete_tree_combo_box = AutocompleteTreeComboBox(self)
        static_text_for_text = wx.StaticText(self, label="What you selected:")
        self.text_ctrl = wx.TextCtrl(self)

        # Events:
        auto_complete_tree_combo_box.Bind(EVT_SELECTION_CHANGED,
                                          self._on_selection_changed)
        # Layout:
        vertical_sizer = wx.BoxSizer(wx.VERTICAL)
        vertical_sizer.AddSpacer(10)
        vertical_sizer.Add(static_text_intro, 0, wx.EXPAND)
        vertical_sizer.AddSpacer(10)
        vertical_sizer.Add(static_text_prereq, 0, wx.EXPAND)
        vertical_sizer.AddSpacer(20)
        vertical_sizer.Add(static_text_for_combo, 0, wx.EXPAND)
        vertical_sizer.AddSpacer(10)
        vertical_sizer.Add(auto_complete_tree_combo_box, 0, wx.EXPAND)
        vertical_sizer.AddSpacer(20)
        vertical_sizer.Add(static_text_for_text, 0, wx.EXPAND)
        vertical_sizer.AddSpacer(10)
        vertical_sizer.Add(self.text_ctrl, 0, wx.EXPAND)
        main_sizer = wx.BoxSizer(wx.HORIZONTAL)
        main_sizer.AddSpacer(20)
        main_sizer.Add(vertical_sizer, 1, wx.EXPAND)
        main_sizer.AddSpacer(20)
        self.SetSizer(main_sizer)

        # Data:
        root = MainWindow._generate_some_data()
        auto_complete_tree_combo_box.set_items(root)

    @staticmethod
    def _generate_some_data():
        """Generates a tree of SomeCustomTreeNode nodes with some bogus test
        data.

        """
        root = SomeCustomTreeNode(-1, "<hidden root>")
        topic1 = root.add_child(SomeCustomTreeNode(10, "Topic 1 yellow"))
        topic1.add_child(SomeCustomTreeNode(11, "Subtopic 1a red green"))
        topic1.add_child(SomeCustomTreeNode(12, "Subtopic 1b green red"))
        topic1.add_child(SomeCustomTreeNode(13, "Subtopic 1c blue red"))
        topic2 = root.add_child(SomeCustomTreeNode(20, "Topic 2 black"))
        topic2.add_child(SomeCustomTreeNode(21, "Subtopic 2a dark red"))
        topic2.add_child(SomeCustomTreeNode(22, "Subtopic 2b dark green"))
        topic2.add_child(SomeCustomTreeNode(23, "Subtopic 2c dark blue"))
        topic3 = root.add_child(SomeCustomTreeNode(90, "Topic 3 purple"))
        topic3.add_child(SomeCustomTreeNode(21, "Subtopic 3a orange"))
        topic3.add_child(SomeCustomTreeNode(22, "Subtopic 3b green"))
        topic3.add_child(SomeCustomTreeNode(23, "Subtopic 3c greenish"))
        return root

    def _on_selection_changed(self, evt):
        """Sets a textual representation of the selected item in the text_ctrl
        text box.
        """
        selected_item = evt.selected_item
        self.text_ctrl.SetValue(str(selected_item))


def main():
    """Creates a wx application and shows the main window."""
    app = wx.App(False)
    frame = MainWindow()
    frame.Show(True)
    app.MainLoop()


if __name__ == '__main__':
    main()
